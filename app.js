const express = require ('express'),
  path = require ('path')
;

const userRouter = require ('./routes/user'),
  todoRouter = require ('./routes/todo')
;

const app = express ()

app.set ('views', path.join (__dirname, 'views'))
app.set ('view engine', 'hbs')

app.use (express.json ())
app.use (express.urlencoded ({ extended: false }))
app.use (express.static (path.join (__dirname, 'public')))

app.use ('/users', userRouter)
app.use ('/todos', todoRouter)

app.listen (5000, () => {
  console.log ("Server started on port https://localhost:5000")
})

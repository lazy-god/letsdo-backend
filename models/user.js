'use strict';
module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define(
    'user', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      nick_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      user_token: {
        type: DataTypes.STRING,
        allowNull: false
      }
    }, 
    {
      indexes: [
        {
          unique: true,
          fields: ['email']
        }
      ]
    }
  );
  
  user.associate = function(models) {
    // associations can be defined here
  };
  
  return user;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  var todo = sequelize.define(
    'todo', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      task: {
        type: DataTypes.STRING,
        allowNull: false
      },
      due_date: {
        type: DataTypes.DATE,
        allowNull: false
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id'
        }
      }
    }, {}
  );
  todo.associate = function({ user, todo }) {
    user.hasMany (todo, { foreignKey: 'userId' })
    todo.belongsTo (user, { foreignKey: 'userId'})
  };
  return todo;
};
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn ('users', 'user_token', {
      type: Sequelize.STRING,
      allowNull: false
    })
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.removeColumn ('users', 'user_token')
  }
};

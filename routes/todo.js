const Express = require ('express'),
  Router = Express.Router (), 
  DB = require ('../models')
;

Router.get ('/all', (request, response, next) => {
  const params = request.query

  DB.user.findOne ({
    where: {
      user_token: params.token
    }
  }).then (user => {
    return DB.todo.findAll ({
      where: {
        userId: user.dataValues.id
      }
    })
  }).then (todos => {
    response.json ({
      todos: todos
    })
  }).catch (err => {
    console.log ('Error: ', err)
    response.json ({
      message: 'error getting all todos'
    })
  })
})

Router.post ('/addTodo', (request, response, next) => {
  const params = request.body

  DB.user.findOne ({
    where: {
      user_token: params.token
    }
  }).then (user => {
    params.userId = user.dataValues.id
    
    return DB.todo.create (params)
  }).then (todo => {
    response.json (todo.dataValues)
  }).catch (err => {
    console.log ('Error: ', err)
    response.json ({
      message: 'error creating todo'
    })
  })
})

Router.patch ('/:id', (request, response, next) => {

})

module.exports = Router;
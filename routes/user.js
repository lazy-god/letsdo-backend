const Express = require ('express'),
  Router = Express.Router (),
  bcrypt = require ('bcrypt'),
  salt = 10,
  DB = require ('../models')
;

Router.post ('/signUp', (request, response, next) => {
  const params = request.body

  bcrypt.hash (params.password, salt, (err, hash) => {
    if (err) {
      console.log ('Error: ', err)
      response.json ({
        message: 'error creating user'
      })
    } else {
      params.password = hash
      
      bcrypt.hash (params.email, salt, (err, hash) => {
        if (err) {
          console.log ('Error: ', err)
          response.json ({
            message: 'error creating user'
          })
        } else {
          params.user_token = hash

          DB.user.create (params)
            .then (user => {
              response.json (user)
            }).catch (err => {
              response.json ({
                message: 'error creating user'
              })
            })
        }
      })
    }
  })
})

Router.post ('/login', (request, response, next) => {
  const params = request.body

  DB.user.findOne ({
    where: {
      email: params.email
    }
  }).then (userData => {
    const user = userData.dataValues

    bcrypt.compare (params.password, user.password, (err, res) => {
      if (err) {
        console.log ('Error: ', err)
        response.json ({
          message: 'error logging user'
        })
      } else if (res) {
        response.json (user)
      } else {
        response.json ({
          message: 'error logging user'
        })
      }
    })
  }).catch (err => {
    console.log ('Error: ', err)
    response.json ({
      message: 'error logging user'
    })
  })
})

module.exports = Router;